#!/usr/bin/php -q
<?php
/**
 * How to run it on console/tty:
 * - Change work dir to up closest to MurtCli.php dir aka "cd .."
 * - Run in your console:
 * $ ./App/MurtCli.php run
 * $ ./App/MurtCli.php run [URI] [METHOD]
 * $ ./App/MurtCli.php run / GET
 * - Parse it to w3m / elinks
 * $ ./App/MurtCli.php run | elinks
 * $ ./App/MurtCli.php run | elinks -dump
 * $ ./App/MurtCli.php run | w3m -T text/html
 * $ ./App/MurtCli.php run | w3m -dump -T text/html
 */


include 'Murt.php';

// // include extra config:
// include 'config.php';

Murt::$base = '';
$routes = array(
    '/' => 'Main',
    '/test/(.+)' => 'Test',
);


$server = $_SERVER;
$method = 'GET';
$uri = '/';

$vars = '';
if (isset($argv[1]) && $argv[1] == 'run') {
    isset($argv[2]) && $uri     = $argv[2];
    isset($argv[3]) && $method  = $argv[3];

    $server['REQUEST_METHOD'] = $method;
    $server['REQUEST_URI'] = $uri;

    Murt::init($routes, $server);
}
unset($server);
