<?php
define('DS', DIRECTORY_SEPARATOR);
header('Content-Type: text/html; charset=UTF-8');

/**
 * Murt 
 * µ Router Template
 * 
 * @author matias \at/ russitto /dot\ com 
 * @license Public Domain, see UNLICENSE file
 * @inspired by GluePHP, Yii and many others
 */
class Murt {
    public static $base = '/';
    public static $method;
    public static $class;
    public static $uri;
    private static $rain;
    public static $template = array(
        'tpl_dir'      => 'App/Templates/',
        'cache_dir'    => 'App/Tmp/RainCache/',
        'debug'        => TRUE,
        'path_replace' => FALSE,
    );
    public static $vars = array(); // extra global variables

    // -- PDO optional DB config
    // // MySQL
    // define('DB_DSN' , 'mysql:dbname=testdb;host=localhost');
    // define('DB_USER', 'your mysql user');
    // define('DB_PASS', 'your mysql pass');
    // // Sqlite
    public static $database = array(
        'dsn'    => 'sqlite:App/Data/app.db', // 'mysql:dbname=testdb;host=localhost'
        'user'   => NULL,
        'pass'   => NULL,
        'freeze' => FALSE,
    );

    public static function init($routes = array(), $server) {
        self::rainConfig();
        self::$method = $method = $server['REQUEST_METHOD'];
        $uri = $server['REQUEST_URI'];
        self::$base == '/' && self::$base = '';
        self::$uri = $uri = preg_replace('/index.php\/?/', '', $uri);
        foreach ($routes as $route => $class) {
            $route == '/' && $route = '';
            self::$class = $class;
            $reg = '|^' . self::$base . $route . '/?$|';
            if (preg_match($reg, $uri, $vars)) {
                // static version
                array_shift($vars);

                // // version >= PHP 5.3
                // $class::$method($vars);
                // version < PHP 5.3
                call_user_func($class . '::' . $method, $vars);

                // // non static version
                // $obj = new $class;
                // $obj->$method($vars);
            }
        }
    }

    public static function render($template = '', $vars = array()) {
        $template == '' && $template = self::$class;
        !isset($vars['_BASE_']) && $vars['_BASE_'] = self::$base;
        !isset($vars['_TPL_' ]) && $vars['_TPL_' ] = self::$base . '/' . self::$template['tpl_dir'];
        self::$rain->assign($vars);
        echo self::$rain->draw($template);
    }

    private static function rainConfig() {
        RainTpl::configure(self::$template);
        self::$rain = new RainTpl;
    }

    public static function e($str) {
        echo $str;
    }
    
    public static function d($str) {
        echo $str;
        die;
    }
    
    public static function de($str) {
        echo '<pre>';
        var_dump($str);
        echo '</pre>';
    }
    
    public static function ded($str) {
        echo '<pre>';
        var_dump($str);
        die;
    }
    
    public static function redir($str) {
        header('Location: ' .  Murt::$base . $str);
    }

    public static function db() {
        if (self::$database['user'] || self::$database['pass'])
            $dbh = R::setup(self::$database['dsn'], self::$database['user'], self::$database['pass']);
        else
            $dbh = R::setup(self::$dbDsn);
        if (self::$database['freeze'])
            R::freeze(self::$database['freeze']);
        return $dbh;
    }

    // autoload µcontrollers
    public static function autoload($class_name) {
        $file = 'App' . DS . 'Controllers' . DS .$class_name . '.php';
        is_file($file) && include $file;
    }
}

spl_autoload_register('Murt::autoload');
