var disqus_shortname = 'russitto-com';
var disqus_identifier = '';
var disqus_url = '';
$(function (){
  if($('#url_id').length){
    // var disqus_shortname = 'russitto-com'; // required: replace example with your forum shortname
    disqus_identifier = $('#url_id').val();
    //console.log(disqus_identifier);
    disqus_url = location.href;
    $.getScript('http://' + disqus_shortname + '.disqus.com/embed.js');
  }

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20570557-2']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_udn', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
});
