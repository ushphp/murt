<?php
// This is /index.php
// And this is Murt:
include 'App/Murt.php';
// If you want to use a db:
include 'App/Vendors/RedBean/rb.php';
// If you want to use templates:
include 'App/Vendors/RainTpl/inc/rain.tpl.class.php';

// // include extra config:
// include 'App/config.php';

// Murt::$base = '/';
// OR:
// Murt::$base = '/other/path';
// OR dinamically:
Murt::$base = dirname($_SERVER['SCRIPT_NAME']);

// Main is a class located at App/Controllers/Main.php
// Test is a class located at App/Controllers/Main.php
$routes = array(
    '/' => 'Main',
    '/test/(\w+)' => 'Test',
);
// Main renders a pure PHP template (some frameworks call it view)
//   at App/Templates/Main.html
// Test renders a pure PHP template (some frameworks call it view)
//   at App/Templates/Test.html

Murt::init($routes, $_SERVER);
